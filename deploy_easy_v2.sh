#!/bin/bash
echo "####################################################################"
echo "Installation Started"
echo "####################################################################"
echo ""
echo ""
PWD=`pwd`
echo "Backup old file"
sudo rm -r /usr/lib/cgi-bin-old
sudo mv /usr/lib/cgi-bin /usr/lib/cgi-bin-old
sudo rm -r /var/www/easy_console/public_html_old
sudo mv /var/www/easy_console/public_html /var/www/easy_console/public_html_old
echo "changing permission of cgi-bin"
sudo chown -R root:root $PWD/cgi-bin
echo "moving cgi-bin and public_html"
sudo mv $PWD/cgi-bin /usr/lib
sudo mv $PWD/public_html /var/www/easy_console/
echo "making all file executable"
sudo find /usr/lib/cgi-bin/ -type f -iname "*.sh" -exec chmod +x {} \;
echo ""
echo ""
echo "####################################################################"
echo "Installation Completed"
echo "####################################################################"
